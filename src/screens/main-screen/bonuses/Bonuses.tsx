import {Block, BlockTitle, Button, Page} from 'framework7-react';
import {useRef, useState} from 'react';
import PromoCodeSheetModal from '../../../modals/PromoCode';
import type {SheetRef} from '../../../modals/PromoCode';
import {SwiperSlideBonuses} from '../../../Components/BonusesSlide';

export const Bonuses = () => {
  const sheetModelRef = useRef<SheetRef>(null);
  const [opened, setOpened] = useState(false);
  return (
    <Page>
      <BlockTitle large className="block-title-without-navbar">
        0 &#8381;
      </BlockTitle>
      <Block>
        <p className="description large">Ваш бонусный счет</p>
      </Block>
      <BlockTitle>Бонусы для Вас</BlockTitle>
      {/* <swiper-container
        className="demo-swiper-multiple demo-swiper-multiple-auto"
        space-between="10"
        slides-per-view="2"
      >
        <SwiperSlideBonuses 
            bgColorGradient='262.19deg, #92ECDC 0%, #4DB5DB 100%'
            title='-10%'
            description='На уборку квартиры
            с кодом UBORKA10'
            image='slide-image-1'
            imagePos='right'
        />
        <SwiperSlideBonuses 
            bgColorGradient='262.19deg, #FABB05 0%, #FF5F00 100%)'
            title='-10%'
            description='На уборку квартиры
            с кодом UBORKA10'
            image='slide-image-1'
            imagePos='left'
        />
        <SwiperSlideBonuses 
            bgColorGradient='82.19deg, #BF92EC 0%, #4DB5DB 100%'
            title='-10%'
            description='На уборку квартиры
            с кодом UBORKA10'
            image='slide-image-1'
            imagePos='right'
        />
      </swiper-container> */}
      <Button onClick={() => sheetModelRef.current?.open()}>Show code</Button>
      <PromoCodeSheetModal ref={sheetModelRef} />
    </Page>
  );
};
