import {
  Block,
  BlockTitle,
  Button,
  Card,
  CardContent,
  Link,
  List,
  ListItem,
  Page,
  Toggle,
} from 'framework7-react';
import {Icon} from '../../../Components/IconELem';

export const Profile = () => {
  return (
    <Page>
      <BlockTitle
        large
        className="
                    block-title-without-navbar 
                    row-container 
                    justify-content-space-between
                "
      >
        Роман
        <Icon icon="editIcon" size="22px" link={true} />
      </BlockTitle>
      <Block>
        <p className="description">+7 (921) 344-89-01</p>
        <Button outline round className="padding-horizontal">
          <div
            className="row-container justify-content-space-between"
            style={{width: '100%'}}
          >
            <p>Бонусный счёт</p>
            <div className="row-container justify-content-space-around">
              <p>1000 &#8381;</p>
              <Icon
                icon="rightArrowIcon"
                width="18px"
                height="15px"
                link={true}
                options="margin-left"
              />
            </div>
          </div>
        </Button>
      </Block>
      <BlockTitle>Настройки</BlockTitle>
      <List className="filled shadowed" inset>
        <ListItem title="romankrestovskiy@gmail.com">
          <Icon icon="emailIcon" size="16px" slot="media" />
          <Icon icon="editIcon" size="22px" link={true} />
        </ListItem>
        <ListItem title="Москва">
          <Icon icon="locationIcon" size="16px" slot="media" />
        </ListItem>
        <ListItem link title="Мои адреса">
          <Icon icon="addressIcon" size="16px" slot="media" />
        </ListItem>
        <ListItem link title="Способы оплаты">
          <Icon icon="walletIcon" size="16px" slot="media" />
        </ListItem>
      </List>
      <BlockTitle>Дополнительно</BlockTitle>
      <List outline inset>
        <ListItem title="Уведомления">
          <Icon icon="notificationsIcon" size="16px" slot="media" />
          <Toggle />
        </ListItem>
        <ListItem title="Мессенджер">
          <Icon icon="bubbleIcon" size="16px" slot="media" />
          <Toggle />
        </ListItem>
        <ListItem link title="О нас">
          <Icon icon="aboutIcon" size="16px" slot="media" />
        </ListItem>
      </List>
      <BlockTitle>Поддержка</BlockTitle>
      <List
        className="padding-bottom padding-top-haf padding-horizontal-half"
        inset
        outline
      >
        <ListItem>
          <div>
            <p className="large-text margin-bottom-half margin-top-half">
              +7 (925) 567-90-20
            </p>
            <p className="description margin-top-half">
              Время работы с 9:00 до 23:00
            </p>
          </div>
          <Icon
            icon="phoneIcon"
            size="22px"
            link={true}
            options="margin-right-half"
          />
        </ListItem>
        <ListItem>
          <Button outline round small className="telegram padding">
            <Icon icon="telegramIcon" size="22px" options="margin-right-half" />
            Telegram
          </Button>
          <Button outline round small className="whatsapp padding">
            <Icon icon="whatsappIcon" size="22px" options="margin-right-half" />
            WhatsApp
          </Button>
        </ListItem>
        <ListItem>
          <Button fill round style={{width: '100%'}}>
            Заказать обратный звонок
          </Button>
        </ListItem>
      </List>
      <Block className="padding-bottom no-margin">
        <Button small className="padding-vertical">
          Выйти из аккаунта
          <Icon
            icon="outIcon"
            size="20px"
            link={true}
            options="margin-left-half"
          />
        </Button>
      </Block>
    </Page>
  );
};
