import {
  Block,
  BlockTitle,
  Button,
  List,
  ListInput,
  Page,
  f7,
} from 'framework7-react';
import React, {Fragment, useState} from 'react';

export const Authorization: React.FC = () => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [inputValue, setInputValue] = useState('');

  const handleInput = (value: string) => {
    const numbers = value
      .split('')
      .filter((character) => character >= '0' && character <= '9');
    const newPhoneNumber = numbers.join('');
    setPhoneNumber(newPhoneNumber);

    let newInputValue = '';
    newPhoneNumber.split('').forEach((number, i) => {
      i === 0
        ? (newInputValue += '(' + number)
        : i === 3
        ? (newInputValue += ') ' + number)
        : i === 6
        ? (newInputValue += '-' + number)
        : i === 8
        ? (newInputValue += '-' + number)
        : (newInputValue += number);
    });
    setInputValue(newInputValue);
  };

  const handleAcceptButton = () => {
    f7.views.main.router.navigate('/Validation', {props: {number: inputValue}});
  };

  return (
    <Fragment>
      <Page className="text-align-center">
        <BlockTitle large className="auth-block-title">
          Вход в аккаунт
        </BlockTitle>
        <Block className="auth-block">
          <p className="description large margin-top">
            Укажите номер мобильного телефона и мы пришлём СМС с кодом для входа
          </p>
          <List className="row-container justify-content-center divider">
            <p className="phone-number-start">+7</p>
            <ListInput
              placeholder="(XXX) XXX-XX-XX"
              validate
              inputmode="numeric"
              autofocus
              onInput={(e) => handleInput(e.target.value)}
              maxlength={'15'}
              className="auth-input auth-input-number"
              value={inputValue}
            ></ListInput>
          </List>
          <Button
            fill
            round
            disabled={phoneNumber.length != 10}
            onClick={() => handleAcceptButton()}
          >
            Прислать СМС
          </Button>
        </Block>
      </Page>
    </Fragment>
  );
};
// export default Authorization
