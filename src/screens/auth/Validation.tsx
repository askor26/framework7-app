import {
  Block,
  BlockTitle,
  Button,
  Link,
  ListInput,
  Page,
  f7,
} from 'framework7-react';
import {Fragment, useState} from 'react';

type ValidationProps = {
  number: string;
};

export const Validation: React.FC<ValidationProps> = ({number}) => {
  const [code, setCode] = useState(['', '', '', '']);
  const [currentFocus, setCurrentFocus] = useState(0);
  const [valid, setValid] = useState(true);

  const handleAcceptButton = () => {
    let concatCode = '';
    code.forEach((value) => (concatCode += value));
    concatCode === '1234'
      ? f7.views.main.router.navigate('/adding-email')
      : setValid(false);
  };

  const changeFocus = (index: number, number: string) => {
    const direction = number === '' ? -1 : 1;
    setCode(code.map((value, i) => (i === index ? number : value)));
    setValid(true);
    document
      .getElementById('code-input-id-' + (currentFocus + direction))
      ?.getElementsByTagName('input')[0]
      .focus();
  };

  return (
    <Fragment>
      <Page className="text-align-center">
        <BlockTitle large className="auth-block-title">
          Введите код
        </BlockTitle>
        <Block className="auth-block">
          <p className="description large margin-top no-margin-bottom">
            Отправили СМС с кодом на номер +7 {number}. Подтверждая номер
            телефона, вы принимаете
          </p>
          <Link>Пользовательское соглашение</Link>
          <ul className="list display-flex flex-direction-row justify-content-space-evenly">
            {code.map((_, i) => {
              return (
                <ListInput
                  placeholder="0"
                  validate
                  inputmode="numeric"
                  key={'code-input-key-' + i}
                  id={'code-input-id-' + i}
                  maxlength={'1'}
                  max={'1'}
                  onInput={(e) => {
                    changeFocus(i, e.target.value);
                  }}
                  onFocus={() => {
                    setCurrentFocus(i);
                  }}
                  className={`auth-input auth-input-code divider margin-horizontal-half ${
                    valid ? '' : 'danger-input'
                  }`}
                />
              );
            })}
          </ul>
          <p className="description danger">
            {valid ? '' : 'Неверно введенный код. Попробуйте еще раз'}
          </p>
          <Button
            fill
            round
            disabled={code.findIndex((value) => value === '') !== -1}
            onClick={() => handleAcceptButton()}
          >
            Подтвердить
          </Button>
        </Block>
      </Page>
    </Fragment>
  );
};
