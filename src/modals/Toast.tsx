import {Icon} from '../Components/IconELem';

export const Toast = () => {
  return (
    <div className="toast toast-bottom toast-horizontal-center">
      <div className="toast-content">
        <div className="toast-text row-container justify-content-space-around">
          <Icon icon="giftIcon" size="50px" link={true} />
          <div style={{textAlign: 'left', maxWidth: '70%'}}>
            <p className="large-text">Промо-код скопирован</p>
            <p className="description">
              Промо-код UBORKA10 скопирован в буфер обмена
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
