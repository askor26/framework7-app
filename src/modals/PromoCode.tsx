import {
  Block,
  BlockTitle,
  Button,
  List,
  ListItem,
  Sheet,
  f7,
} from 'framework7-react';
import {Sheet as SheetType} from 'framework7/types';
import {forwardRef, useImperativeHandle, useState} from 'react';
import {Icon} from '../Components/IconELem';
import ReactDomServer from 'react-dom/server';
import {Toast} from './Toast';
import {Share} from '@capacitor/share';

export type SheetRef = {
  open: () => void;
  close: () => void;
} | null;
type SheetProps = {};

const PromoCodeSheetModal = forwardRef((props: SheetProps, ref) => {
  const [opened, setOpened] = useState(false);

  useImperativeHandle(ref, () => ({
    open: () => open(),
    close: () => close(),
  }));

  const open = () => {
    setOpened(true);
  };

  const close = () => {
    setOpened(false);
  };

  const handleCopy = () => {
    close();
    f7.toast
      .create({
        closeTimeout: 3000,
        position: 'bottom',
        render: () => {
          return ReactDomServer.renderToString(<Toast />);
        },
      })
      .open();
  };

  const handleShareButton = async (promocode: string) => {
    await Share.share({
      title: 'Промокод',
      text: `Привет, держи промокод: ${promocode}!`,
      dialogTitle: 'Твой промокод',
    });
  };

  return (
    <Sheet
      backdrop
      opened={opened}
      onSheetClosed={() => setOpened(false)}
      swipeToClose={true}
      swipeHandler=".swipe-handler"
    >
      <div className="swipe-handler" />
      <BlockTitle large>-10% на уборку квартиры</BlockTitle>
      <List className="filled shadowed no-padding" inset>
        <ListItem link noChevron={true} onClick={() => handleCopy()}>
          <div slot="media" className="row-container">
            <p className="margin-right-half">Промо-код:</p>
            <p className="large-text">UBORKA10</p>
          </div>
          <span slot="after">
            <Icon icon="copyIcon" size="25px" link={true} />
          </span>
        </ListItem>
      </List>
      <Block>
        <p className="description large">
          Скопируйте данный промо-код и получите скидку на уборку в размере 10%
          от общей суммы заказа. Промо-код действителен месяц с момента
          размещения на платформе 19.10.2021.
        </p>
        <p className="description large long-text">
          Также вы можете поделиться с вашими друзьями данным кодом. Просто
          нажмите кнопку “Поделиться” ниже
        </p>
        <Button fill round onClick={() => handleShareButton('UBORKA10')}>
          Поделиться с друзьями
        </Button>
      </Block>
    </Sheet>
  );
});

export default PromoCodeSheetModal;
