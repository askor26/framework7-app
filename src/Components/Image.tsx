import React from "react";

type ImageProps = {
    image: string
    width: string
    height: string
    slot?: string
    options?: string
}


export const Image:React.FC<ImageProps> = ({image,width,height,slot,options}) =>{
    return(
        <div
            slot={slot}
            className={`image ${options?options:''}`}
            style={{
                backgroundImage: `url("${require(`../assets/images/${image}.png`)}")`,
                width: width,
                height: height,
            }}
        />
    );
}

