import { Block, Link, Navbar, Page } from "framework7-react";
import React, { Fragment } from "react";

type PageProps = {
    pageId: string,
}


const ReadMoreHolder:React.FunctionComponent<PageProps> = ({pageId}) => {
    return(
        <Fragment>
            <Navbar title={'Page '+pageId}>
                <Link back slot='left'>Back</Link>
            </Navbar>
            <Page>
                <Block>
                    <p>Text</p>
                </Block>
            </Page>
        </Fragment>
    );
}
export default ReadMoreHolder