import React, { useEffect } from "react";

type IconProps = {
    icon: string
    size?: string
    width?: string
    height?: string
    link?: boolean
    slot?: string
    options?: string
}


export const Icon:React.FC<IconProps> = ({icon,size='0px',width,height,link,slot,options}) =>{
    return(
        <i
            slot={slot}
            className={`icon ${link?'icon-link':''} ${options?options:''}`}
            style={{
                maskImage: `url("${require(`../assets/icons/${icon}.svg`)}")`,
                WebkitMaskImage: `url("${require(`../assets/icons/${icon}.svg`)}")`,
                width: width?width:size,
                height: height?height:size,
            }}
        />
    );
}