import {Image} from './Image';

type Props = {
  bgColorGradient: string;
  image: string;
  title: string;
  description: string;
  imagePos: string;
};

export const SwiperSlideBonuses: React.FC<Props> = ({
  bgColorGradient,
  image,
  title,
  description,
  imagePos,
}) => {
  return (
    <swiper-slide
      style={{
        background: `linear-gradient(${bgColorGradient})`,
      }}
    >
      <div className="row-container">
        <Image
          image={image}
          width="100px"
          height="100px"
          options={imagePos == 'right' ? 'float-right' : ''}
        />
        <div>
          <p className="large-text">{title}</p>
          <p className="description">{description}</p>
        </div>
      </div>
    </swiper-slide>
  );
};
