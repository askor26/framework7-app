import {Block, Button, Navbar, Page} from 'framework7-react';
import React, {Fragment} from 'react';
import CardElem from './Card';
export default () => {
  const cardsData = ['Card 1', 'Card 2', 'Card 3'];
  return (
    <Fragment>
      <Navbar title="Tab 1" />
      <Page>
        <CardElem />
        <CardElem />
        <CardElem />
        <Block>
          <Button
            href="/tab-1/about"
            view="#tab-1"
            text="About"
            large
            fillIos
            fillMd
          />
        </Block>
      </Page>
    </Fragment>
  );
};
