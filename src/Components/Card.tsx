import React from 'react';
import {
  BlockTitle,
  Card,
  CardHeader,
  CardContent,
  CardFooter,
  Link,
} from 'framework7-react';

const CardElem = () => {
  return (
    <>
      <BlockTitle>Styled Cards</BlockTitle>
      <Card outlineMd className="demo-card-header-pic">
        <CardHeader
          style={{
            backgroundImage:
              'url(https://cdn.framework7.io/placeholder/nature-1000x600-3.jpg)',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            width: '100%',
            height: 0,
            paddingTop: '66%',
          }}
        >
          Journey To Mountains
        </CardHeader>
        <CardContent>
          <p className="date">Posted on January 21, 2015</p>
          <p>
            Quisque eget vestibulum nulla. Quisque quis dui quis ex ultricies
            efficitur vitae non felis. Phasellus quis nibh hendrerit...
          </p>
        </CardContent>
        <CardFooter>
          <Link>Like</Link>
          <Link>Read more</Link>
        </CardFooter>
      </Card>
    </>
  );
};

export default CardElem;
