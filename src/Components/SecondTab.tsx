import {
  Block,
  BlockTitle,
  Card,
  CardContent,
  Link,
  List,
  ListItem,
  NavRight,
  Navbar,
  Page,
  PageContent,
  Popup,
  Tab,
  View,
} from 'framework7-react';
import React, {Fragment} from 'react';

export default () => {
  const listItems = ['Item 1', 'Item 2', 'Item 3'];
  return (
    <Fragment>
      <Navbar title="Tab 2" />
      <Page>
        <BlockTitle>Our advantages</BlockTitle>
        <Card>
          <CardContent padding={false} />
          <List linksList>
            {listItems.map((item, index) => (
              <ListItem key={'list-item-key-' + index}>
                <Link text={item} popupOpen={'#list-item-popup-' + index} />
              </ListItem>
            ))}
          </List>
        </Card>
      </Page>

      {listItems.map((item, index) => (
        <Popup
          id={'list-item-popup-' + index}
          key={'list-item-popup-key-' + index}
        >
          <View router={false}>
            <Page>
              <Navbar title={`Advantage ${index + 1}`}>
                <NavRight>
                  <Link popupClose>Close</Link>
                </NavRight>
              </Navbar>
              <Block>
                <p>
                  {
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ligula leo, malesuada eu lorem pulvinar, laoreet lobortis lectus. Donec ullamcorper auctor lectus eget faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus'
                  }
                </p>
              </Block>
            </Page>
          </View>
        </Popup>
      ))}
    </Fragment>
  );
};
