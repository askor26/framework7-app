import {Link, Toolbar, View, Views} from 'framework7-react';
import {Fragment} from 'react';
import {Icon} from './IconELem';

type MainLaoutProps = {
  authorized: boolean;
};

const MainLayout: React.FC<MainLaoutProps> = ({authorized}) => {
  return (
    <Fragment>
      <Views tabs>
        <Toolbar bottom tabbar icons className="main-toolbar">
          <Link tabLink href="#tab-1" tabLinkActive>
            <Icon icon="homeIcon" size="25px" />
          </Link>
          <Link tabLink href="#tab-2">
            <Icon icon="ordersIcon" size="25px" />
          </Link>
          <Link tabLink href="#tab-3">
            <Icon icon="bonusesIcon" size="25px" />
          </Link>
          <Link tabLink href="#tab-4">
            <Icon icon="profileIcon" size="25px" />
          </Link>
        </Toolbar>
        <View tab id="tab-1" url="/tab-1/firstTab" tabActive />
        <View tab id="tab-2" url="/tab-2/SecondTab" />
        <View tab id="tab-3" url="/bonuses/" />
        <View tab id="tab-4" url="/profile/" />
      </Views>
    </Fragment>
  );
};

export default MainLayout;
