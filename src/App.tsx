import {App, View} from 'framework7-react';
import {createContext, useState} from 'react';

import SecondTab from './Components/SecondTab';
import FirstTab from './Components/FirstTab';
import About from './Components/About';
import ReadMoreHolder from './Components/ReadMore';

import {
  Authorization,
  Validation,
  AddingEmail,
  Profile,
  Bonuses,
} from './screens';
import MainLayout from './Components/MainLayout';

type ContextType = {
  setAuthorized?: (auth: boolean) => void;
};

const AppContext: React.Context<ContextType> = createContext({});

const f7params = {
  name: 'My App',
  theme: 'auto',
  routes: [
    {
      path: '/',
      component: MainLayout,
    },
    {
      path: '/authorization',
      component: Authorization,
    },
    {
      path: '/profile/',
      component: Profile,
    },
    {
      path: '/validation',
      component: Validation,
    },
    {
      path: '/adding-email',
      component: AddingEmail,
    },
    {
      path: '/tab-1/about',
      component: About,
    },
    {
      path: '/tab-1/firstTab',
      component: FirstTab,
    },
    {
      path: '/tab-2/secondTab',
      component: SecondTab,
    },
    {
      path: '/tab-1/:pageId/',
      component: ReadMoreHolder,
    },
    {
      path: '/bonuses/',
      component: Bonuses,
    },
  ],
};

export {AppContext};
// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  const [authorized, setAuthorized] = useState(false);

  return (
    <App {...f7params}>
      <AppContext.Provider value={{setAuthorized: setAuthorized}}>
        <View main url={authorized ? '/' : '/authorization/'} />
      </AppContext.Provider>
    </App>
  );
};
