import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';
import reportWebVitals from './reportWebVitals';
import Framework7 from 'framework7/lite/bundle';
import Framework7React from 'framework7-react';
import 'framework7/css/bundle';
import 'framework7-icons';
import './App.css';
import { register } from 'swiper/element/bundle';
import { SplashScreen } from '@capacitor/splash-screen';

Framework7.use(Framework7React);
register();

declare global {
  namespace JSX {
      interface IntrinsicElements {
          'swiper-container': React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
          'swiper-slide': React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
      }
  }
}

const showSplashScreen = async () => {
  await SplashScreen.show({
    showDuration: 2000,
    autoHide: true,
  });
}

showSplashScreen();

const root = ReactDOM.createRoot(
  document.getElementById('app') as HTMLElement
);
root.render(
  // <React.StrictMode>
    <App />
  // </React.StrictMode> 
);

// ReactDOM.render(
//   React.createElement(App),
//   document.getElementById('app')
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
